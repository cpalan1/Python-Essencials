# -*- coding: utf-8 -*-
"""
Created on Sun Nov  7 09:50:02 2021

@author: cpala
"""

print ("Empezando a trabajar con Python")
print ("Realizado por: CARLOS PALAN")

print("Consultando los tipos de valores:")

V1=str("El tipo de dato de 875 es:")
L1=V1.split()
T1=int(L1[5])
print(V1,type(T1)," NUMERO ENTERO")

V2=str("El tipo de dato de 4.89 es:")
L2=V2.split()
T2=float(L2[5])
print(V2,type(T2)," NUMERO PUNTO FLOTANTE")

V3=str("El tipo de dato del texto: Now is better than never es:")
L3=V3[27:40]
T3=type(V3)
print(V3,T3," STRING")

V4=str("El tipo de dato de 1.32 es:")
L4=V4.split()
T4=float(L4[5])
print(V4,type(T4)," NUMERO PUNTO FLOTANTE")

V5=str("¿El valor 5+8i corresponde a un valor entero?:")
L5=V5[9:13]
T5=L5 is int()
print(V5,T5," NO ES ENTERO")

V6=str("¿El valor 8.2 corresponde a un valor entero?:")
L6=V6[9:12]
T6=L6 is int()
print(V6,T6," NO ES ENTERO")

V7=str("¿El texto: Readability counts. corresponde a un string?")
L7=V7[11:29]
T7=str(L7)
print(V7,type(T7)," SI, ES UN STRING ")