# -*- coding: utf-8 -*-
"""
Created on Sun Nov  7 11:43:05 2021

@author: cpala
"""

print("Programa que identifica el tipo de dato de un valor ingresado por el usuario, se realizarán cinco interacciones:")
print ("Realizado por: CARLOS PALAN")

print()
print("Primera Interacción, ingrese un valor cualquiera: ")
X1=input() #El usuario debe ingresar un valor
print("Este tipo de dato en Python es:")
print(type (X1))

print()
print("Segunda Interacción, ingrese un valor cualquiera: ")
X2=input() #El usuario debe ingresar un valor
print("Este tipo de dato en Python es:")
print(type (X2))

print()
print("Tercera Interacción, ingrese un valor cualquiera: ")
X3=input() #El usuario debe ingresar un valor
print("Este tipo de dato en Python es:")
print(type (X3))

print()
print("Cuarta Interacción, ingrese un valor cualquiera: ")
X4=input() #El usuario debe ingresar un valor
print("Este tipo de dato en Python es:")
print(type (X4))

print()
print("Quinta Interacción, ingrese un valor cualquiera: ")
X5=input() #El usuario debe ingresar un valor
print("Este tipo de dato en Python es:")
print(type (X5))

print()
print("¡YA NO SE HARÁN MÁS INTERACCIONES!4")